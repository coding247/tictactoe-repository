import bootState from './states/boot'
import loadState from './states/load'
import playState from './states/play'
import winState from './states/win'
import loseState from './states/lose'
import drawState from './states/draw'
import overState from './states/over'

game.state.add('boot', bootState)
game.state.add('load', loadState)
game.state.add('play', playState)
game.state.add('win', winState)
game.state.add('lose', loseState)
game.state.add('draw', drawState)
game.state.add('over', overState)

game.state.start('boot')
