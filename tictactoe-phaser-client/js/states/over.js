const overState = {
  create () {
    // restart the game when iconDecide is received
    socket.on('iconDecide', (obj) => {
      // resetting game global data
      game.global = {
        totalWon: 0,
        totalLost: 0,
        totalDrawn: 0
      }
      // starting the play state with an init value
      game.state.start('play', true, false, obj.iAmX)
    })

    game.stage.backgroundColor = '#000000'

    // display game over message
    const drawMessage = game.add.text(
      game.world.centerX, 200, 'Game Over',
      { font: '50px Arial', fill: '#ffffff' }
    )
    drawMessage.anchor.setTo(0.5, 0.5)

    const startGame = game.add.text(
      game.world.centerX, 250, 'Your opponent has left. Waiting for a player to join this arena.',
      { font: '20px Arial', fill: '#ffffff' }
    )
    startGame.anchor.setTo(0.5, 0.5)
  }
}

export default overState
