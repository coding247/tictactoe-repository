/* globals: game, receiveMove, socketHelpers, sendMove */
const playState = {
  init (iAmX) {
    // if an init value was passed
    if (iAmX !== undefined) this.iAmX = iAmX
  },
  create () {
    // initializing socket
    receiveMove.call(this)
    socketHelpers.call(this)

    // true if the client is X, false if the client is O. Dynamically assigned by the server
    this.iAmX

    // set game background image
    this.statsBoxHeight = 40
    this.gameBackground = game.add.sprite(0, this.statsBoxHeight, 'level-background')
    this.gameBackground.inputEnabled = true

    this.x = 0
    this.playerCanSetX = this.iAmX || true
    this.cellWidth = 186
    this.cellHeight = 186
    // getting a 2 dimensional array filled with all 0s
    this.template = this.get2dArray(3, 3, 0)

    // game stats text style
    const statsBoxStyle = {
      font: 'Georgia',
      fill: '#ffffff',
      align: 'center' }
    // adding statistics texts
    this.gameText = this.iAmX ? 'Your turn' : 'Waiting for your turn...'
    this.gameStatus = game.add.text(15, 15, this.gameText, statsBoxStyle)
    game.add.text(470, 7, 'Total Won: ' + game.global.totalWon, statsBoxStyle)
    game.add.text(470, 20, 'Total Lost: ' + game.global.totalLost, statsBoxStyle)

    this.playerCanSetX ? this.gameBackground.events.onInputDown.add(this.setX, this) : this.gameBackground.events.onInputDown.add(this.setO, this)
  },

  update () {
    if ((this.iAmX && !this.playerCanSetX) || (!this.iAmX && this.playerCanSetX)) {
      this.gameStatus.text = 'Waiting for your turn...'
    }

    if ((this.iAmX && this.playerCanSetX) || (!this.iAmX && !this.playerCanSetX)) {
      this.gameStatus.text = 'Your turn'
    }

    // before the game begins
    if (this.iAmX === undefined) {
      this.gameStatus.text = 'Waiting for the other player to join'
    }
  },

  /**
   * @param {Int} rows Number of rows.
   * @param {Int} columns Number of columns.
   * @param {Any} el The whole array is filled with the 'el' value in the beginning.
   * @returns {Array}
   */
  get2dArray (rows, columns, el) {
    return Array(rows).fill().map(() => Array(columns).fill(el))
  },

  /**
   * @param {Int} indexX The X coordinate of the input.
   * @param {Int} indexY The Y coordinate of the input.
   * @param {Boolean} autoRender True if automatically rendered upon receiveMove. False if renderd by a click.
   * @returns {Void}
   */
  setX (phaserPointer, pointerEvent, indexX, indexY, autoRender) {
    if (this.iAmX || autoRender !== undefined) {
      // Calculate the position on the grid it translates to
      var cellIndexX = indexX === undefined ? Math.floor(this.gameBackground.toLocal(game.input).x / this.cellWidth) : indexX
      var cellIndexY = indexY === undefined ? Math.floor(this.gameBackground.toLocal(game.input).y / this.cellHeight) : indexY
      if (this.playerCanSetX && !this.template[cellIndexY][cellIndexX]) {
        this.template[cellIndexY][cellIndexX] = 'x'
        game.add.image(cellIndexX * this.cellWidth, cellIndexY * this.cellHeight + this.statsBoxHeight, 'x')
        this.x += 1
        this.checkMatch()
        if (!autoRender) {
          // sending the move to the other client
          let obj = {
            coords: {
              x: cellIndexX,
              y: cellIndexY
            },
            icon: 'x'
          }
          sendMove(obj)
        }
        // switching side and onDown function
        this.playerCanSetX = false
        game.input.onDown.add(this.setO, this)
      }
    }
  },

  /**
   * @param {Int} indexX The X coordinate of the input.
   * @param {Int} indexY The Y coordinate of the input.
   * @param {Boolean} autoRender True if automatically rendered upon receiveMove. False if rendered by a click.
   * @returns {Void}
   */
  setO (phaserPointer, pointerEvent, indexX, indexY, autoRender) {
    if (!this.iAmX || autoRender !== undefined) {
      // Calculate the position on the grid it translates to
      var cellIndexX = indexX === undefined ? Math.floor(this.gameBackground.toLocal(game.input).x / this.cellWidth) : indexX
      var cellIndexY = indexY === undefined ? Math.floor(this.gameBackground.toLocal(game.input).y / this.cellHeight) : indexY
      if (!this.playerCanSetX && !this.template[cellIndexY][cellIndexX]) {
        this.template[cellIndexY][cellIndexX] = 'o'
        game.add.image(cellIndexX * this.cellWidth, cellIndexY * this.cellHeight + this.statsBoxHeight, 'o')
        this.checkMatch()
        if (!autoRender) {
          // sending the move to the other client
          let obj = {
            coords: {
              x: cellIndexX,
              y: cellIndexY
            },
            icon: 'o'
          }
          sendMove(obj)
        }
        // switching side and onDown function
        this.playerCanSetX = true
        game.input.onDown.add(this.setX, this)
      }
    }
  },

  /**
   * @description Receives the winner name and triggers state based on that.
   * @param {String} icon The string of the winner icon.
   * @returns {Void}
   */
  winner (icon) {
    if (icon === 'x' && this.iAmX) {
      // changing icon
      this.iAmX = !this.iAmX
      // adding to gamestats
      game.global.totalWon++
      // showing the result
      game.state.start('win')
    } else if (icon === 'o' && !this.iAmX) {
      this.iAmX = !this.iAmX
      game.global.totalWon++
      game.state.start('win')
    } else {
      this.iAmX = !this.iAmX
      game.global.totalLost++
      game.state.start('lose')
    }
  },

  /**
   * @description Receives the 2D board array and returns the game result.
   * @param {Array} board The 2D array of the board.
   * @returns {String} The game result.
   */
  evaluator (board) {
    board = board.join('-').replace(/,/g, '')
    if (/ooo|o...o...o|o....o....o|o..o..o/.test(board)) return 'o'
    if (/xxx|x...x...x|x....x....x|x..x..x/.test(board)) return 'x'
    if (/0/.test(board)) return 'unfinished'
    return 'draw'
  },

  /**
   * @description Gets called on every move. Checks the game result.
   * @returns {Void}
   */
  checkMatch () {
    let board = this.template
    let result = this.evaluator(board)
    switch (result) {
      case 'x':
        this.winner('x')
        break
      case 'o':
        this.winner('o')
        break
      case 'draw':
        game.state.start('draw')
        break
      default:
        break
    }
  }
}

export default playState
