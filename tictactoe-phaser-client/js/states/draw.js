const drawState = {
  create () {
    game.stage.backgroundColor = '#000000'

    // display draw message
    const drawMessage = game.add.text(
      game.world.centerX, 200, 'Game Drawn',
      { font: '50px Arial', fill: '#ffffff' }
    )
    drawMessage.anchor.setTo(0.5, 0.5)

    const startGame = game.add.text(
      game.world.centerX, 250, 'Game restarting...',
      { font: '20px Arial', fill: '#ffffff' }
    )
    startGame.anchor.setTo(0.5, 0.5)

    // restarting the game after 1 second
    setTimeout(() => {
      this.startGame()
    }, 1000)
  },
  startGame () {
    game.state.start('play')
  }
}

export default drawState
