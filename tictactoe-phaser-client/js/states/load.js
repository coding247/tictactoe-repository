const loadState = {
  preload () {
    game.load.path = 'images/'
    game.load.images(['level-background', 'x', 'o', 'cell'])
  },
  create () {
    game.state.start('play')
  }
}

export default loadState
