const loseState = {
  create () {
    game.stage.backgroundColor = '#000000'

    // display lose message
    const loseMessage = game.add.text(
      game.world.centerX, 200, 'You lost :(',
      { font: '50px Arial', fill: '#ffffff' }
    )
    loseMessage.anchor.setTo(0.5, 0.5)

    const startGame = game.add.text(
      game.world.centerX, 250, 'Game restarting...',
      { font: '20px Arial', fill: '#ffffff' }
    )
    startGame.anchor.setTo(0.5, 0.5)

    // restarting the game after 1 second
    setTimeout(() => {
      this.startGame()
    }, 1000)
  },
  startGame () {
    game.state.start('play')
  }
}

export default loseState
