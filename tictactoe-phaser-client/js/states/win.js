const winState = {
  create () {
    game.stage.backgroundColor = '#000000'

    // display won message
    const wonMessage = game.add.text(
      game.world.centerX, 200, 'WINNER WINNER!',
      { font: '50px Arial', fill: '#ffffff' }
    )
    wonMessage.anchor.setTo(0.5, 0.5)

    const startGame = game.add.text(
      game.world.centerX, 250, 'Game restarting...',
      { font: '20px Arial', fill: '#ffffff' }
    )
    startGame.anchor.setTo(0.5, 0.5)

    // starting the game after 1 second
    setTimeout(() => {
      this.startGame()
    }, 1000)
  },
  startGame () {
    game.state.start('play')
  }
}

export default winState
