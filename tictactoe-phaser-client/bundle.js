'use strict';

(function () {
  'use strict';

  var bootState = {
    create: function create() {
      if (!game.device.desktop) {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        document.body.style.backgroundColor = '#ffffff';

        game.scale.minHeight = 186;
        game.scale.minWidth = 186;

        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;

        game.scale.updateLayout(true);
      }

      game.state.start('load');
    }
  };

  var loadState = {
    preload: function preload() {
      game.load.path = 'images/';
      game.load.images(['level-background', 'x', 'o', 'cell']);
    },
    create: function create() {
      game.state.start('play');
    }
  };

  /* globals: game, receiveMove, socketHelpers, sendMove */
  var playState = {
    init: function init(iAmX) {
      // if an init value was passed
      if (iAmX !== undefined) this.iAmX = iAmX;
    },
    create: function create() {
      // initializing socket
      receiveMove.call(this);
      socketHelpers.call(this);

      // true if the client is X, false if the client is O. Dynamically assigned by the server
      this.iAmX;

      // set game background image
      this.statsBoxHeight = 40;
      this.gameBackground = game.add.sprite(0, this.statsBoxHeight, 'level-background');
      this.gameBackground.inputEnabled = true;

      this.x = 0;
      this.playerCanSetX = this.iAmX || true;
      this.cellWidth = 186;
      this.cellHeight = 186;
      // getting a 2 dimensional array filled with all 0s
      this.template = this.get2dArray(3, 3, 0);

      // game stats text style
      var statsBoxStyle = {
        font: 'Georgia',
        fill: '#ffffff',
        align: 'center' };
      // adding statistics texts
      this.gameText = this.iAmX ? 'Your turn' : 'Waiting for your turn...';
      this.gameStatus = game.add.text(15, 15, this.gameText, statsBoxStyle);
      game.add.text(470, 7, 'Total Won: ' + game.global.totalWon, statsBoxStyle);
      game.add.text(470, 20, 'Total Lost: ' + game.global.totalLost, statsBoxStyle);

      this.playerCanSetX ? this.gameBackground.events.onInputDown.add(this.setX, this) : this.gameBackground.events.onInputDown.add(this.setO, this);
    },
    update: function update() {
      if (this.iAmX && !this.playerCanSetX || !this.iAmX && this.playerCanSetX) {
        this.gameStatus.text = 'Waiting for your turn...';
      }

      if (this.iAmX && this.playerCanSetX || !this.iAmX && !this.playerCanSetX) {
        this.gameStatus.text = 'Your turn';
      }

      // before the game begins
      if (this.iAmX === undefined) {
        this.gameStatus.text = 'Waiting for the other player to join';
      }
    },


    /**
     * @param {Int} rows Number of rows.
     * @param {Int} columns Number of columns.
     * @param {Any} el The whole array is filled with the 'el' value in the beginning.
     * @returns {Array}
     */
    get2dArray: function get2dArray(rows, columns, el) {
      return Array(rows).fill().map(function () {
        return Array(columns).fill(el);
      });
    },


    /**
     * @param {Int} indexX The X coordinate of the input.
     * @param {Int} indexY The Y coordinate of the input.
     * @param {Boolean} autoRender True if automatically rendered upon receiveMove. False if renderd by a click.
     * @returns {Void}
     */
    setX: function setX(phaserPointer, pointerEvent, indexX, indexY, autoRender) {
      if (this.iAmX || autoRender !== undefined) {
        // Calculate the position on the grid it translates to
        var cellIndexX = indexX === undefined ? Math.floor(this.gameBackground.toLocal(game.input).x / this.cellWidth) : indexX;
        var cellIndexY = indexY === undefined ? Math.floor(this.gameBackground.toLocal(game.input).y / this.cellHeight) : indexY;
        if (this.playerCanSetX && !this.template[cellIndexY][cellIndexX]) {
          this.template[cellIndexY][cellIndexX] = 'x';
          game.add.image(cellIndexX * this.cellWidth, cellIndexY * this.cellHeight + this.statsBoxHeight, 'x');
          this.x += 1;
          this.checkMatch();
          if (!autoRender) {
            // sending the move to the other client
            var obj = {
              coords: {
                x: cellIndexX,
                y: cellIndexY
              },
              icon: 'x'
            };
            sendMove(obj);
          }
          // switching side and onDown function
          this.playerCanSetX = false;
          game.input.onDown.add(this.setO, this);
        }
      }
    },


    /**
     * @param {Int} indexX The X coordinate of the input.
     * @param {Int} indexY The Y coordinate of the input.
     * @param {Boolean} autoRender True if automatically rendered upon receiveMove. False if rendered by a click.
     * @returns {Void}
     */
    setO: function setO(phaserPointer, pointerEvent, indexX, indexY, autoRender) {
      if (!this.iAmX || autoRender !== undefined) {
        // Calculate the position on the grid it translates to
        var cellIndexX = indexX === undefined ? Math.floor(this.gameBackground.toLocal(game.input).x / this.cellWidth) : indexX;
        var cellIndexY = indexY === undefined ? Math.floor(this.gameBackground.toLocal(game.input).y / this.cellHeight) : indexY;
        if (!this.playerCanSetX && !this.template[cellIndexY][cellIndexX]) {
          this.template[cellIndexY][cellIndexX] = 'o';
          game.add.image(cellIndexX * this.cellWidth, cellIndexY * this.cellHeight + this.statsBoxHeight, 'o');
          this.checkMatch();
          if (!autoRender) {
            // sending the move to the other client
            var obj = {
              coords: {
                x: cellIndexX,
                y: cellIndexY
              },
              icon: 'o'
            };
            sendMove(obj);
          }
          // switching side and onDown function
          this.playerCanSetX = true;
          game.input.onDown.add(this.setX, this);
        }
      }
    },


    /**
     * @description Receives the winner name and triggers state based on that.
     * @param {String} icon The string of the winner icon.
     * @returns {Void}
     */
    winner: function winner(icon) {
      if (icon === 'x' && this.iAmX) {
        // changing icon
        this.iAmX = !this.iAmX;
        // adding to gamestats
        game.global.totalWon++;
        // showing the result
        game.state.start('win');
      } else if (icon === 'o' && !this.iAmX) {
        this.iAmX = !this.iAmX;
        game.global.totalWon++;
        game.state.start('win');
      } else {
        this.iAmX = !this.iAmX;
        game.global.totalLost++;
        game.state.start('lose');
      }
    },


    /**
     * @description Receives the 2D board array and returns the game result.
     * @param {Array} board The 2D array of the board.
     * @returns {String} The game result.
     */
    evaluator: function evaluator(board) {
      board = board.join('-').replace(/,/g, '');
      if (/ooo|o...o...o|o....o....o|o..o..o/.test(board)) return 'o';
      if (/xxx|x...x...x|x....x....x|x..x..x/.test(board)) return 'x';
      if (/0/.test(board)) return 'unfinished';
      return 'draw';
    },


    /**
     * @description Gets called on every move. Checks the game result.
     * @returns {Void}
     */
    checkMatch: function checkMatch() {
      var board = this.template;
      var result = this.evaluator(board);
      switch (result) {
        case 'x':
          this.winner('x');
          break;
        case 'o':
          this.winner('o');
          break;
        case 'draw':
          game.state.start('draw');
          break;
        default:
          break;
      }
    }
  };

  var winState = {
    create: function create() {
      var _this = this;

      game.stage.backgroundColor = '#000000';

      // display won message
      var wonMessage = game.add.text(game.world.centerX, 200, 'WINNER WINNER!', { font: '50px Arial', fill: '#ffffff' });
      wonMessage.anchor.setTo(0.5, 0.5);

      var startGame = game.add.text(game.world.centerX, 250, 'Game restarting...', { font: '20px Arial', fill: '#ffffff' });
      startGame.anchor.setTo(0.5, 0.5);

      // starting the game after 1 second
      setTimeout(function () {
        _this.startGame();
      }, 1000);
    },
    startGame: function startGame() {
      game.state.start('play');
    }
  };

  var loseState = {
    create: function create() {
      var _this2 = this;

      game.stage.backgroundColor = '#000000';

      // display lose message
      var loseMessage = game.add.text(game.world.centerX, 200, 'You lost :(', { font: '50px Arial', fill: '#ffffff' });
      loseMessage.anchor.setTo(0.5, 0.5);

      var startGame = game.add.text(game.world.centerX, 250, 'Game restarting...', { font: '20px Arial', fill: '#ffffff' });
      startGame.anchor.setTo(0.5, 0.5);

      // restarting the game after 1 second
      setTimeout(function () {
        _this2.startGame();
      }, 1000);
    },
    startGame: function startGame() {
      game.state.start('play');
    }
  };

  var drawState = {
    create: function create() {
      var _this3 = this;

      game.stage.backgroundColor = '#000000';

      // display draw message
      var drawMessage = game.add.text(game.world.centerX, 200, 'Game Drawn', { font: '50px Arial', fill: '#ffffff' });
      drawMessage.anchor.setTo(0.5, 0.5);

      var startGame = game.add.text(game.world.centerX, 250, 'Game restarting...', { font: '20px Arial', fill: '#ffffff' });
      startGame.anchor.setTo(0.5, 0.5);

      // restarting the game after 1 second
      setTimeout(function () {
        _this3.startGame();
      }, 1000);
    },
    startGame: function startGame() {
      game.state.start('play');
    }
  };

  var overState = {
    create: function create() {
      // restart the game when iconDecide is received
      socket.on('iconDecide', function (obj) {
        // resetting game global data
        game.global = {
          totalWon: 0,
          totalLost: 0,
          totalDrawn: 0
        };
        // starting the play state with an init value
        game.state.start('play', true, false, obj.iAmX);
      });

      game.stage.backgroundColor = '#000000';

      // display game over message
      var drawMessage = game.add.text(game.world.centerX, 200, 'Game Over', { font: '50px Arial', fill: '#ffffff' });
      drawMessage.anchor.setTo(0.5, 0.5);

      var startGame = game.add.text(game.world.centerX, 250, 'Your opponent has left. Waiting for a player to join this arena.', { font: '20px Arial', fill: '#ffffff' });
      startGame.anchor.setTo(0.5, 0.5);
    }
  };

  game.state.add('boot', bootState);
  game.state.add('load', loadState);
  game.state.add('play', playState);
  game.state.add('win', winState);
  game.state.add('lose', loseState);
  game.state.add('draw', drawState);
  game.state.add('over', overState);

  game.state.start('boot');
})();