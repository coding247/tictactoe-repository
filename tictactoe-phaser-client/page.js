'use strict';

/* global functions */

/**
 * @description Returns the socket correct socket hostname based on production or developement.
 * @returns {String} The correct hostname
 */
function getSocketHostName() {
  var socketHostName;
  var hostname = window.location.hostname;
  if (hostname === 'tictactoe-client.herokuapp.com') {
    socketHostName = 'https://tictactoe-server-js.herokuapp.com';
  } else {
    socketHostName = 'localhost:3000';
  }
  return socketHostName;
}

/**
 * @description Sends the given object to the server.
 * @param {Object} obj The object cosisting of the coordinates.
 * @returns {Void}
 */
function sendMove(obj) {
  socket.emit('move', obj);
}

/**
 * @description Receive the moves from the server.
 * @returns {Void}
 */
function receiveMove() {
  var _this = this;

  socket.on('receiveMove', function (obj) {
    if (obj.icon === 'x') {
      _this.setX(null, null, obj.coords.x, obj.coords.y, true);
    } else if (obj.icon === 'o') {
      _this.setO(null, null, obj.coords.x, obj.coords.y, true);
    }
  });
}

/**
 * @description Takes the room name list from the object and adds it as an UL element to the DOM.
 * @param {Object} obj The object from the socket.io 'receiveRoomNames' event.
 * @returns {Void}
 */
function receiveRoomNamesHandler(obj) {
  var roomListDiv = document.getElementById('room-list');
  if (obj.roomNamesArray.length > 0) {
    // clearing the room-list div
    roomListDiv.innerHTML = '';
    // creating a new UL DOM Element from the array
    var generatedUL = ulFromArray(obj.roomNamesArray);
    // appending the generated UI
    roomListDiv.appendChild(generatedUL);
    // adding event listeners to all li elements
    document.querySelectorAll('#room-list ul li').forEach(function (elem) {
      elem.addEventListener('click', function () {
        socket.emit('roomName', { name: elem.textContent });
        document.getElementById('tictactoe').style.display = 'block';
        document.getElementById('tictactoe-room-input').style.display = 'none';
      });
    });
  } else {
    // if there's no item in the roomNamesArray
    roomListDiv.innerHTML = 'No rooms available';
  }
}

/**
 * @description Socket.io related helper function.
 * @returns {Void}
 */
function socketHelpers() {
  var _this2 = this;

  socket.on('iconDecide', function (obj) {
    _this2.iAmX = obj.iAmX;
  });

  socket.on('gameOver', function () {
    game.state.start('over');
  });
}

/**
 * @description Homepage input toggler. Once the rooname is submitted it hides the input field and unhide the game area.
 * @returns {Void}
 */
function roomNameHandler() {
  var room = document.getElementById('room-name').value;
  if (!room) {
    alert('Enter a valid Arena name');
  } else {
    socket.emit('roomName', { name: room });
    document.getElementById('tictactoe').style.display = 'block';
    document.getElementById('tictactoe-room-input').style.display = 'none';
  }
}

/**
 * @description Creates a bootstrap ready UL from an array
 * @param {Array} array Array of the item names
 * @returns {Element} DOM Element containing the generated UL
 */
function ulFromArray(array) {
  if (array.length < 1) return false;
  var list = document.createElement('ul');
  list.classList.add('list-group');
  for (var i = 0; i < array.length; i++) {
    var item = document.createElement('li');
    item.classList.add('list-group-item');
    item.appendChild(document.createTextNode(array[i]));
    list.appendChild(item);
  }
  return list;
}

// initializing the game object
var game = new Phaser.Game(560, 600, Phaser.AUTO, 'tictactoe');
// global game data
game.global = {
  totalWon: 0,
  totalLost: 0,
  totalDrawn: 0

  // initializing socket
};var hostName = getSocketHostName();
var socket = io(hostName);

socket.on('receiveRoomNames', receiveRoomNamesHandler);
//  adding the event listener to button
document.getElementById('room-name-button').addEventListener('click', roomNameHandler);