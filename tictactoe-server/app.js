const app = require('express')()
const server = require('http').Server(app)
const io = require('socket.io')(server)

// created modules
const getUserCreatedRooms = require('./helpers/user-created-rooms')
const getRoomNamesWithOnePlayer = require('./helpers/room-names-with-one-player')

const PORT = process.env.PORT || 3000

function getRoomNames () {
  var userCreatedRooms = getUserCreatedRooms(io)
  return getRoomNamesWithOnePlayer(userCreatedRooms)
}

// test
app.get('/test', (req, res) => {
  res.send('hello world test')
})

io.on('connection', (socket) => {
  var roomName
  var connectedClients

  // sending the room names to the new client
  var roomNamesArray = getRoomNames()
  socket.emit('receiveRoomNames', {
    roomNamesArray
  })

  socket.on('roomName', (obj) => {
    roomName = obj.name
    socket.join(roomName)
    // sending the updated room names to all clients
    var roomNamesArray = getRoomNames()
    io.emit('receiveRoomNames', {
      roomNamesArray
    })

    // list of connected clients in the room
    connectedClients = Object.keys(io.sockets.adapter.rooms[roomName].sockets)

    // for the first two players
    if (connectedClients.length === 2) {
      // sending the icon decision
      io.to(connectedClients[0]).emit('iconDecide', { iAmX: true })
      io.to(connectedClients[1]).emit('iconDecide', { iAmX: false })
    }
  })

  socket.on('move', (obj) => {
    socket.to(roomName).emit('receiveMove', obj)
  })

  // handling disconnection
  socket.on('disconnect', () => {
    socket.to(roomName).emit('gameOver', { isGameOver: true })
    // send the updated room list to everyone when someone disconnects
    var roomNamesArray = getRoomNames()
    io.emit('receiveRoomNames', {
      roomNamesArray
    })
  })
})

// code starts
server.listen(PORT, () => {
  console.log(`The server is listening on port ${PORT}`)
})
