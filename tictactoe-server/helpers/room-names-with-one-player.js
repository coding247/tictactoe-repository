/**
 * @description Returns the names of the rooms that have only one player in them.
 * @param {Array} roomList An array of objects containing information about rooms.
 * @returns {Array} Returns an array of the names of the rooms that have only one player in them.
 */
function getRoomNamesWithOnePlayer (roomList) {
  if (!roomList) throw new Error('The roomList argument can not be empty')
  var roomName = []
  roomList.forEach((elem) => {
    if (elem.data.length === 1) roomName.push(elem.roomName)
  })
  return roomName
}
module.exports = getRoomNamesWithOnePlayer
