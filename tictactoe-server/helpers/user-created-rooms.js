/**
 * @description Takes the reference of a namespace as an argument and returns an array of objects containing information about the rooms created manually.
 * @param {Object} namespace The reference to a namespace
 * @returns {Array} Array of objects containing information about the rooms created manually
 */
function getUserCreatedRooms (namespace) {
  if (!namespace) throw new Error('The namespace argument is not set or not valid.')
  var userCreatedRooms = []
  var allRooms = namespace.sockets.adapter.rooms
  var sids = namespace.sockets.adapter.sids
  Object.keys(allRooms).forEach((elem) => {
    if (!sids.hasOwnProperty(elem)) {
      userCreatedRooms.push({ roomName: elem, data: allRooms[elem] })
    }
  })
  return userCreatedRooms
}
module.exports = getUserCreatedRooms
